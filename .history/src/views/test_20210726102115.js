import {LitElement, html} from 'lit-element';
class TodoItem extends LitElement {
  static get properties() {
    return {
      item: {type: String},
    };
  }

  render() {
    return html`
      <div class="todo-item">
        <span>${this.item}</span>
      </div>
    `;
  }
}

customElements.define('todo-item', TodoItem);