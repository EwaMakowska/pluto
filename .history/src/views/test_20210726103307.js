import {LitElement, html} from 'lit-element';
class MyElement extends LitElement {
  static get properties() {
    return {
      item: {type: String},
    };
  }

  var items = "??????????????????????????",

  render() {
    return html`
      <div class="todo-item">
        <span>${this.item}</span>
      </div>
    `;
  }
}

customElements.define('my-element', MyElement);