const path = require('path');

module.exports = {
  entry: {
    'index-js': './src/index.js',
    'index-scss': './src/scss/index.scss',
    'index': './index.html'
  }
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'bundle.js',
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: ["style-loader", "css-loader", "postcss-loader"]
      },
      {
        test: /\.s[ac]ss$/i,
        use: [
          "style-loader",
          "css-loader",
          "sass-loader"
        ],
      },
    ]
  }
};