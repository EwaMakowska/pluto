const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');

module.exports = {
  entry: {
  app: './src/index.js'
},
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'bundle.js',
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: ["style-loader", "css-loader", "postcss-loader"]
      },
      {
        test: /\.s[ac]ss$/i,
        use: [
          {
            loader: "style-loader",
            options: { 
              modules: true,
              sourceMap: true,
             },
          },
          "css-loader",
          "sass-loader"
        ],
      },
    ]
  },
  plugins: [new HtmlWebpackPlugin( {filename: 'index.html', template: 'index.html'} )],
};