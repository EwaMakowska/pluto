const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');

module.exports = {
  devServer: {
    hot: true
  },
  entry: {
  app: './src/index.js'
},
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'bundle.js',
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: ["style-loader", "css-loader", "postcss-loader"]
      },
      {
        test: /\.s[ac]ss$/i,
        use: [
          {
            loader: "style-loader",
            options: { 
              modules: true,
              sourceMap: true,
             },
          },
          "css-loader",
          {
            loader: "sass-loader",
            options: {
              includePaths: [`src/scss`],
            }
          }
          
        ],
      },
    ]
  },
  plugins: [new HtmlWebpackPlugin( {filename: 'index.html', template: 'index.html'} )],
};