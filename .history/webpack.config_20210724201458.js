const HtmlWebpackPlugin = require('html-webpack-plugin');
const path = require('path');

module.exports = {
  entry: {
  app: './src/index.js'
},
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'bundle.js',
  },
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        use: ["babel-loader"]
      },
      {
        test: /\.css$/,
        use: ["style-loader", "css-loader", "postcss-loader"]
      },
      {
        test: /\.scss$/,
        use: [
          'style-loader',
          MiniCssExtractPlugin.loader,
          {
              loader: "css-loader",
              options: {
                  minimize: true,
                  sourceMap: true
              }
          },
          {
              loader: "sass-loader"
          }
      ]
      },
    ]
  },
  plugins: [
    new HtmlWebpackPlugin( {filename: 'index.html', template: 'index.html'} ),
        new MiniCssExtractPlugin({
        filename: "[name].css",
        chunkFilename: "[id].css"
    })
  
  ],
};