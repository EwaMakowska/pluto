const HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const path = require("path");
const WebpackShellPluginNext = require('webpack-shell-plugin-next');
const spawn = require('child_process').spawn;

module.exports = {
  devtool: true,
  devServer: {
    hot: true,
    index: 'index.html',
  },
  entry: {
    app: "./src/index.js",
  },
  output: {
    path: path.resolve(__dirname, "dist"),
    filename: "bundle.js",
    sourceMapFilename: "[name].js.map"
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: ["style-loader", "css-loader"],
      },
      {
				test: /\.scss$/,
				use: [MiniCssExtractPlugin.loader, 'style-loader', "css-loader", "sass-loader"]
      },
    ],
  },
  plugins: [
    new WebpackShellPluginNext({
      onBuildStart:{
        scripts: ['node-sass --output-style compressed -o dist src/scss'],
        blocking: true,
        parallel: false
      }, 
      onBuildEnd:{
        scripts: ['echo "Webpack End"'],
        blocking: false,
        parallel: true
      }
    }),
    new HtmlWebpackPlugin({ filename: "index.html", template: "index.html" }),
    new MiniCssExtractPlugin({
      filename: "[name].css",
      chunkFilename: "[id].css",
    }),
  ],
};
